/**
 * @file: Comment Live.
 */

(function ($) {

Drupal.behaviors.commentLive = {
  // Run when DOM loaded.
  attach: function (context, settings) {
    if ($("#comment-form #edit-preview", context).length) {
      var id;

      // Only update when user hasn't typed anything for one second.
      $("#comment-form textarea", context).on('keyup', function() {
        clearTimeout(id);
        id = setTimeout(function() {
          $("#edit-preview").mousedown();
        }, 1000);
      });
    }

    // When a reply link is clicked, move the form and set the parent.
    $(".comment-reply a").on('click', function() {

      // Remove the PID input field if there is one.
      $("#ajaxPid").remove();

      // Get the PID of the clicked on reply link.
      var commentForm = $("#comment-form").addClass('indented'),
          href = $(this).attr('href'),
          pid = href.substring(href.lastIndexOf("/") + 1);

      // Append the PID as a form value.
      commentForm.append('<input type="hidden" name="ajaxPid" id="ajaxPid" value="' + pid + '" />');

      // Plase the form after the parent comment.
      $(this).closest(".comment").after(commentForm);
      return false;
    });
  }
};
})(jQuery);
